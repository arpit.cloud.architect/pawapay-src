from bottle import route, run, debug, default_app, response
import os
import random
from requests import get
import mysql.connector
from mysql.connector import Error
dbhost="pawapay.cvcfceqfssb0.us-east-1.rds.amazonaws.com"
database="ips"
user="user"
port=3306
password="YourPwdShouldBeLongAndSecure!"
ip=""
@route('/')
def index():
    try:
        connection = mysql.connector.connect(host=dbhost,
					    ssl_disabled=True,
                                            user=user,
                                            port = port,
                                            password=password)
        if connection.is_connected():
            db_Info = connection.get_server_info()
            print("Connected to MySQL Server version ", db_Info)
            cursor = connection.cursor()
            sql1="CREATE DATABASE IF NOT EXISTS ips;"
            cursor.execute(sql1)
            connection.commit()
            connection_new = mysql.connector.connect(host=dbhost,
                                            database=database,
                                            user=user,
					   ssl_disabled=True,
                                            port = port,
                                            password=password)
            cursor = connection_new.cursor()
            sql2="CREATE TABLE IF NOT EXISTS listip (id INT, ip VARCHAR(20));"
            cursor.execute(sql2)
            connection_new.commit()
            return '<h2>Hello</h2>'

    except Error as e:
        print("Error while connecting to MySQL", e)
        return '<h2>DB connection failed</h2>'
    

@route('/client-ip')
def client_ip():
    try:
        connection = mysql.connector.connect(host=dbhost,
                                            database=database,
                                            user=user,
                                            port = port,
					    ssl_disabled=True,
                                            password=password)
        if connection.is_connected():
            db_Info = connection.get_server_info()
            print("Connected to MySQL Server version ", db_Info)
            cursor = connection.cursor()
            ip = get('https://api.ipify.org').text
            r = random.randint(0,255)
            sql="INSERT INTO listip (id,ip) VALUES(%s, %s)"
            val=(r,str(ip))
            cursor.execute(sql,val)
            connection.commit()

    except Error as e:
        print("Error while connecting to MySQL", e)
    return '<h2>'+ip+'</h2>'

@route('/client-ip/list')
def ip_list():
    try:
        connection = mysql.connector.connect(host=dbhost,
                                            database=database,
                                            user=user,
					    ssl_disabled=True,
                                            port = port,
                                            password=password)
        if connection.is_connected():
            db_Info = connection.get_server_info()
            print("Connected to MySQL Server version ", db_Info)
            cursor = connection.cursor()
            cursor.execute("select database();")
            record = cursor.fetchone()
            print("You're connected to database: ", record)

    except Error as e:
        print("Error while connecting to MySQL", e)
    cursor.execute("SELECT * FROM listip;")
    records = cursor.fetchall()
    str="<h1>List of ip</h1><br>"
    for row in records:
        str+="<h2>IP="+row[1]+"<br>"
    
    str+="<br>"    
    return str


app = default_app()

debug(True)
run(host='0.0.0.0', port=8000, reloader=True)
