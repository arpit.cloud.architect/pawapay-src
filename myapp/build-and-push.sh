set -xe
docker build --platform linux/amd64 -t pawaypay-image/payment .
aws ecr --region us-east-1 get-authorization-token --output text --query 'authorizationData[].authorizationToken' --profile arpit_cred | base64 -d | cut -d: -f2 > token
SOURCE_REPOSITORY=966756065099.dkr.ecr.us-east-1.amazonaws.com/pawaypay-image/payment
cat token | docker login $SOURCE_REPOSITORY --username AWS --password-stdin
docker tag pawaypay-image/payment:latest $SOURCE_REPOSITORY:latest
docker push $SOURCE_REPOSITORY:latest
rm token